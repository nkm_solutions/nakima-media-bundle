<?php

use PHPUnit\Framework\TestCase;

class MainTest extends TestCase
{
    public function testAdd()
    {
        $a = 8;
        $b = 7;

        $main = new \Nakima\MediaBundle\Main;

        $this->assertEquals(15, $main->add($a, $b));
    }

}
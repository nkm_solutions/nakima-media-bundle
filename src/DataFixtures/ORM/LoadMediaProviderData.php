<?php
declare(strict_types=1);
namespace Nakima\MediaBundle\DataFixtures\ORM;

/**
 * @author jgonzalez@nakima.es
 */

use MediaBundle\Entity\MediaProvider;
use Nakima\CoreBundle\DataFixtures\ORM\Fixture;

class LoadMediaProviderData extends Fixture
{

    public function loadProd(): void
    {
        $mediaProvider = new MediaProvider();
        $mediaProvider->setName('FILE');
        $this->persist($mediaProvider, 'mediaprovider-file');

        $mediaProvider = new MediaProvider();
        $mediaProvider->setName('IMAGE');
        $this->persist($mediaProvider, 'mediaprovider-image');

        $mediaProvider = new MediaProvider();
        $mediaProvider->setName('PDF');
        $this->persist($mediaProvider, 'mediaprovider-pdf');

    }

    public function loadDev(): void
    {
        $this->loadProd();
    }

    public function loadTest(): void
    {
        $this->loadProd();
    }

}

<?php
declare(strict_types=1);

namespace Nakima\MediaBundle\Entity;

/**
 * @author xgonzalez@nakima.es
 */

use Doctrine\ORM\Mapping\Column;
use Doctrine\ORM\Mapping\JoinColumn;
use Doctrine\ORM\Mapping\ManyToOne;
use Doctrine\ORM\Mapping\MappedSuperclass;
use MediaBundle\Entity\MediaProvider;
use Nakima\ControllerBundle\Utils\SymfonyHelper;
use Nakima\CoreBundle\Entity\BaseEntity;
use Nakima\CoreBundle\Utils\Doctrine;
use Nakima\CoreBundle\Utils\Symfony;
use Symfony\Component\HttpFoundation\File\File;

/**
 * @MappedSuperclass(repositoryClass="Nakima\MediaBundle\Repository\MediaRepository")
 */
class Media extends BaseEntity
{

    const METADATA_EMPTY = "{}";
    //const PROVIDERS = ['file', 'image', 'video', 'tumblr',  'youtube', 'vimeo', 'imgur'];

    /**
     * @var string
     * @Column(type="string", length=128)
     */
    protected $path;

    /**
     * @var MediaProvider
     * @ManyToOne(targetEntity="MediaBundle\Entity\MediaProvider")
     * @JoinColumn(name="mediaprovider_id", referencedColumnName="id")
     */
    protected $provider;

    /**
     * @var array
     * @Column(type="json")
     */
    protected $metadata;

    /**
     * @var array
     * @Column(type="json_save")
     */
    protected $privateMetadata;

    /**
     * @var int
     * @Column(type="integer")
     */
    protected $size;

    /**
     * @var string
     * @Column
     */
    protected $mime;

    /**
     * @var string
     * @Column(length=128)
     */
    protected $name;

    protected $file;

    protected $link;

    /**************************************************************************
     * Custom Functions                                                       *
     **************************************************************************/

    public function __toArray(array $options = []): array
    {
        $ret = parent::__toArray($options);

        $ret['path']         = $this->getPath();
        $ret['name']         = $this->getName();
        $ret['fullpath']     = $this->getFullpath();
        $ret['provider']     = $this->getProvider();
        $ret['mime']         = $this->getMime();
        $ret['metadata']     = $this->getMetadata();
        $ret['size']         = $this->getSize();
        $ret['downloadLink'] = $this->getDownloadLink();

        return $ret;
    }

    public function __construct()
    {
        $this->provider = 'provider.file';
        $this->setMetadata([]);
        $this->setPrivateMetadata([]);

        $this->setPath("/bundles/nakimamedia/img");
        $this->setProvider(\MediaBundle\Entity\MediaProvider::IMAGE());
        $this->setName('default.jpg');
        $this->setMime('image/jpeg');
        $this->setSize(7592);
    }

    public function __toString()
    {
        return $this->name;
    }

    public function getDownloadLink(string $filename = null): ?string
    {
        if (!$filename) {
            $filename = $this->getName();
        }

        return Symfony::get('router')->generate(
            "nakima_media_media_download",
            ['id' => $this->id, 'filename' => $filename],
            0
        );
    }

    public function getPath(): string
    {
        return $this->path;
    }

    public function setPath(string $path)
    {
        $this->path = $path;
    }

    public function getProvider(): ?MediaProvider
    {
        return $this->provider;
    }

    public function setProvider(?MediaProvider $provider)
    {
        $this->provider = $provider;
    }

    /**
     * @return array
     */
    public function getMetadata(): array
    {
        return $this->metadata;
    }

    /**
     * @param array $metadata
     */
    public function setMetadata(array $metadata)
    {
        $this->metadata = $metadata;
    }

    /**
     * @return array
     */
    public function getPrivateMetadata(): array
    {
        return $this->privateMetadata;
    }

    /**
     * @param array $privateMetadata
     */
    public function setPrivateMetadata(array $privateMetadata)
    {
        $this->privateMetadata = $privateMetadata;
    }

    public function getSize(): int
    {
        return $this->size;
    }

    public function setSize(int $size)
    {
        $this->size = $size;
    }

    public function getMime(): string
    {
        return $this->mime;
    }

    public function setMime(string $mime)
    {
        $this->mime = $mime ?? 'unknown';
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name)
    {
        $this->name = $name;
    }

    public function deleteFile()
    {

        $file = $this->getPrivateMetadata()["dir"];

        if (file_exists($file)) {
            unlink($file);
        }
    }

    public function upload(string $folder, string $filename, string $dir = null)
    {

        if (!$dir) {
            $dir = $folder;
        }

        if (!$this->getFile()) {
            return false;
        }

        $root = Symfony::getRoot();
        $this->setName($filename);

        $meta             = $this->getPrivateMetadata();
        $meta['dir']      = "$folder";
        $meta['filename'] = "$filename";
        $meta['full']     = "$folder/$filename";
        $this->setPrivateMetadata($meta);

        $this->getFile()->move(
            "$root/$folder",
            $filename
        );
    }

    public function getFile(): ?File
    {
        return $this->file;
    }

    public function setFile(File $file)
    {
        if ($file) {
            $this->file = $file;

            $this->setMime($file->getMimeType());
            $this->setProvider(
                \MediaBundle\Entity\MediaProvider::loadFromMime($this->getMime())
            );
            $this->setSize($file->getSize());
            $this->setMetadata([]);
            $this->setPrivateMetadata([]);
        }

        return $this;
    }

    public function getCompletePath(): string
    {
        return $this->path . "/" . $this->name;
    }

    public function getFullpath(): string
    {
        return "$this->path/$this->name";
    }

}

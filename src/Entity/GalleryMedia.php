<?php
declare(strict_types=1);
namespace Nakima\MediaBundle\Entity;

/**
 * @author xgonzalez@nakima.es
 */

use Doctrine\ORM\Mapping\Column;
use Doctrine\ORM\Mapping\JoinColumn;
use Doctrine\ORM\Mapping\ManyToOne;
use Doctrine\ORM\Mapping\MappedSuperclass;
use Nakima\CoreBundle\Entity\BaseEntity;
use Nakima\CoreBundle\Utils\Doctrine;

/**
 * @MappedSuperclass(repositoryClass="Nakima\MediaBundle\Repository\GalleryMediaRepository")
 */
class GalleryMedia extends BaseEntity
{

    /**
     * @Column(type="integer")
     */
    protected $position;

    /**
     * @ManyToOne(targetEntity="Gallery", inversedBy="galleryMedias")
     * @JoinColumn(name="gallery_id", referencedColumnName="id")
     */
    protected $gallery;

    /**
     * @ManyToOne(targetEntity="Media", cascade={"all"})
     * @JoinColumn(name="media_id", referencedColumnName="id", onDelete="Cascade")
     */
    protected $media;

    /**************************************************************************
     * Custom Functions                                                       *
     **************************************************************************/

    public function __toArray(array $options = []): array
    {
        return [
            'id' => $this->getId(),
            'position' => $this->getPosition(),
            'media' => Doctrine::toArray($this->getMedia()),
        ];
    }

    /**************************************************************************
     * Getters & Setters                                                      *
     **************************************************************************/

    public function getPosition()
    {
        return $this->position;
    }

    public function setPosition($position)
    {
        $this->position = $position;

        return $this;
    }

    public function getMedia()
    {
        return $this->media;
    }

    public function setMedia($media)
    {
        $this->media = $media;

        return $this;
    }

    public function getGallery()
    {
        return $this->gallery;
    }

    public function setGallery($gallery)
    {
        $this->gallery = $gallery;

        return $this;
    }

}

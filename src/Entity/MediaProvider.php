<?php
declare(strict_types=1);
namespace Nakima\MediaBundle\Entity;

/**
 * @author xgonzalez@nakima.es
 */

use Doctrine\ORM\Mapping\MappedSuperclass;
use Nakima\ControllerBundle\Utils\SymfonyHelper;
use Nakima\CoreBundle\Entity\BaseStatusEntity;
use Nakima\CoreBundle\Utils\Symfony;
use Nakima\Utils\String\Text;

/**
 * @MappedSuperclass()
 */
class MediaProvider extends BaseStatusEntity
{


    /**
     * constants
     */

    static private $IMAGE;

    public static function IMAGE()
    {
        $doctrine = Symfony::get('doctrine');
        self::$IMAGE = $doctrine->getRepository("MediaBundle:MediaProvider")->findOneByName('IMAGE');

        return self::$IMAGE;
    }

    static private $FILE;

    public static function FILE()
    {
        $doctrine = Symfony::get('doctrine');
        self::$FILE = $doctrine->getRepository("MediaBundle:MediaProvider")->findOneByName('FILE');

        return self::$FILE;
    }

    static private $PDF;

    public static function PDF()
    {
        $doctrine = Symfony::get('doctrine');
        self::$PDF = $doctrine->getRepository("MediaBundle:MediaProvider")->findOneByName('PDF');

        return self::$PDF;
    }


    public static function loadFromMime($mime)
    {

        if (Text::startsWith($mime, "image")) {
            return self::load("IMAGE");
        }

        if ($mime === "application/pdf") {
            return self::load("PDF");
        }

        return self::load("FILE");
    }

}

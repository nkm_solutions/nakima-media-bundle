<?php
declare(strict_types=1);
namespace Nakima\MediaBundle\Entity;

/**
 * @author xgonzalez@nakima.es
 */

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping\MappedSuperclass;
use Doctrine\ORM\Mapping\OneToMany;
use Nakima\CoreBundle\Entity\BaseEntity;
use Nakima\CoreBundle\Utils\Doctrine;

/**
 * @MappedSuperclass(
 *     repositoryClass="Nakima\MediaBundle\Repository\GalleryRepository"
 * )
 */
class Gallery extends BaseEntity
{

    /**
     * @OneToMany(
     *     targetEntity="MediaBundle\Entity\GalleryMedia",
     *     mappedBy="gallery",
     *     cascade={"all"}
     * )
     */
    protected $galleryMedias;

    /**************************************************************************
     * Custom Functions                                                       *
     **************************************************************************/

    public function __toArray(array $options = []): array
    {
        return [
            'id' => $this->getId(),
            'galleryMedias' => Doctrine::toArray($this->getGalleryMedias()),
        ];
    }

    /**************************************************************************
     * Getters & Setters                                                      *
     **************************************************************************/

    public function __construct()
    {
        parent::__construct();
        $this->galleryMedias = new ArrayCollection;
    }

    public function addGalleryMedia($galleryMedia)
    {

        foreach ($this->getGalleryMedias() as $key => $value) {
            if ($value->getMedia()->getId() == $galleryMedia->getMedia()->getId()) {
                return;
            }
        }

        $this->galleryMedias[] = $galleryMedia;
        $galleryMedia->setGallery($this);
        $galleryMedia->setPosition((count($this->galleryMedias)));

        return $this;
    }

    public function addMedia($media)
    {

        foreach ($this->getGalleryMedias() as $key => $value) {
            if ($value->getMedia()->getId() == $media->getId()) {
                return;
            }
        }
        $galleryMedia = new \MediaBundle\Entity\GalleryMedia;
        $galleryMedia->setMedia($media);

        $this->addGalleryMedia($galleryMedia);

        return $this;
    }

    public function removeGalleryMedia($galleryMedia)
    {
        $this->galleryMedias->removeElement($galleryMedia);

        return $this;
    }

    public function getGalleryMedias()
    {
        return $this->galleryMedias;
    }

}

<?php
declare(strict_types=1);
namespace Nakima\MediaBundle\Controller;

/**
 * @author xgc1986 < xgc1986@gmail.com >
 */

use Nakima\CoreBundle\Controller\BaseController;
use Nakima\CoreBundle\Utils\Doctrine;
use Nakima\CoreBundle\Utils\Symfony;
use Nakima\Utils\String\Text;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class MediaController extends BaseController
{

    public function uploadAction(Request $request)
    {

        $this->checkMethod("POST");
        $user = $this->checkUser();

        $files = $request->files->all();
        $file = null;
        foreach ($files as $key => $value) {
            $file = $value;
        }

        $userId = $user->getId();
        $hash = Text::rstr(8);
        $extension = $file->guessExtension();
        $filename = "$userId.$hash.$extension";
        $root = Symfony::getRoot();

        $provider = $this->get('nakima.media.provider');
        $media = $provider->buildMedia($file, 'file');
        $media->setPath("/uploads/files");
        $media->setName($filename);
        $media->upload(
            "/web/uploads/files",
            $filename
        );

        $manager = $this->getDoctrine()->getManager();
        $manager->persist($media);
        $manager->flush();

        return new JsonResponse(
            [
                'media' => Doctrine::toArray($media),
            ]
        );
    }


    public function downloadAction(Request $request, $id, $filename)
    {
        $this->checkMethod("GET");

        $media = $this->getRepo("MediaBundle:Media")->findOneById($id);
        if (!$media) {
            throw $this->createNotFoundException();
        }

        $base = Symfony::getRoot()."/web".$media->getFullpath();
        $route = $base;

        if (isset($media->getPrivateMetadata()["full"])) {
            if (file_exists($media->getPrivateMetadata()["full"])) {
                $route = $media->getPrivateMetadata()["full"];
            } else {
                $route = Symfony::getRoot()."/".$media->getPrivateMetadata()["full"];
            }
        }

        return new BinaryFileResponse($route);
    }
}

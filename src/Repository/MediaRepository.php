<?php
declare(strict_types=1);
namespace Nakima\MediaBundle\Repository;

/**
 * @author xgonzalez@nakima.es
 */

use Nakima\CoreBundle\Repository\BaseRepository;

class MediaRepository extends BaseRepository
{

    protected function transform($entity, $translator = null, $format = 'json')
    {
        return [
            'id' => $entity->getId(),
            'path' => $entity->getPath(),
            'provider' => $entity->getProvider(),
            'metadata' => $entity->getMetadata(),
            'filetype' => $entity->getFiletype(),
            'name' => $entity->getname(),
        ];
    }
}

<?php
declare(strict_types=1);
namespace Nakima\MediaBundle\Repository;

/**
 * @author xgonzalez@nakima.es
 */

use Nakima\CoreBundle\Repository\BaseRepository;

class GalleryRepository extends BaseRepository
{

    protected function transform($entity, $translator = null, $format = 'json')
    {
        return [
            'id' => $entity->getId(),
            'medias' => $this->getRepo("MediaBundle:GalleryMedia")->transformEntity(
                $entity->getGalleryMedias(),
                $translator,
                $format
            ),
        ];
    }
}

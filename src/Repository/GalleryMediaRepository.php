<?php
declare(strict_types=1);
namespace Nakima\MediaBundle\Repository;

/**
 * @author xgonzalez@nakima.es
 */

use Nakima\CoreBundle\Repository\BaseRepository;

class GalleryMediaRepository extends BaseRepository
{

    protected function transform($entity, $translator = null, $format = 'json')
    {
        $ret = [
            'position' => $entity->getPosition(),
        ];

        $ret = array_merge(
            $ret,
            $this->getRepo("MediaBundle:GalleryMedia")->transform($entity->getMedia(), $translator, $format)
        );

        return $ret;
    }
}

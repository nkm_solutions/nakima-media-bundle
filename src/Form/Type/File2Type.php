<?php
declare(strict_types=1);
namespace Nakima\MediaBundle\Form\Type;

/**
 * xgc1986 < xgc1986@gmail.com >
 */

use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormView;
use Symfony\Component\OptionsResolver\OptionsResolver;

class File2Type extends AbstractType
{

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            [
                'data_class' => null,
                'class' => 'MediaBundle\Entity\Media',
                'compound' => false,
                'route' => 'nakima_media_media',
            ]
        );

        //$resolver->setDefined(['route']);
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
    }

    public function getParent()
    {
        return EntityType::class;
    }

    public function buildView(FormView $view, FormInterface $form, array $options)
    {
        $view->vars['route'] = $options['route'];
    }
}

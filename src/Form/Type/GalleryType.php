<?php
declare(strict_types=1);
namespace Nakima\MediaBundle\Form\Type;

/**
 * xgc1986 < xgc1986@gmail.com >
 */

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class GalleryType extends AbstractType
{

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            [
                'data_class' => 'MediaBundle\Entity\Gallery',
                'compound' => true,
            ]
        );
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('file', 'hidden');
        $builder->add('media', 'hidden');
    }

    public function getParent()
    {
        return FileType::class;
    }
}

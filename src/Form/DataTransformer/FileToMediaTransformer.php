<?php
declare(strict_types=1);
namespace Nakima\MediaBundle\Form\DataTransformer;

/**
 * @author xgc1986 < xgc1986@gmail.com >
 * @author Adrià Llaudet Planas < allaudet@nakima.es >
 */

use Nakima\CoreBundle\Utils\Symfony;
use Nakima\Utils\String\Text;
use Symfony\Component\Form\DataTransformerInterface;

class FileToMediaTransformer implements DataTransformerInterface
{

    protected $mediaProviderService;
    protected $entity;
    protected $field;
    protected $path;
    protected $name;
    protected $provider;
    protected $public;

    public function __construct($entity, $field, $path, $name, $provider, $public, $mediaProviderService)
    {
        $this->mediaProviderService = $mediaProviderService;
        $this->entity = $entity;
        $this->field = $field;
        $this->path = $path;
        $this->name = $name;
        $this->provider = $provider;
        $this->public = $public;

        if (!$this->name) {
            $this->name = Text::rstr(12);
        }
    }

    public function transform($file)
    {
        return $file;
    }

    public function reverseTransform($file)
    {
        if (!$file) {
            return $this->entity->get($this->field);
        }

        if (!$file->getFile()) {
            return $this->entity->get($this->field);
        }

        $id = $this->name;
        $file = $file->getFile();

        $root = Symfony::getRoot();
        $extension = ".".$file->guessExtension();
        $path = $this->path;
        $filename = $id.$extension;

        $media = $this->mediaProviderService->buildMedia($file, $this->provider, $this->entity->get($this->field));
        $media->setPath("/uploads$path");
        $media->setName($filename);

        if ($this->public) {
            $media->upload(
                "$root/web/uploads$path",
                $filename
            );
        } else {
            $media->upload(
                "$root/uploads$path",
                $filename
            );
        }

        return $media;
    }
}

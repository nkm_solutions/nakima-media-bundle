<?php
declare(strict_types=1);
namespace Nakima\MediaBundle;

/**
 * @author xgonzalez
 */

use Symfony\Component\HttpKernel\Bundle\Bundle;

class NakimaMediaBundle extends Bundle {
}

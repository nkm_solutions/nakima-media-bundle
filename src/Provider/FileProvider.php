<?php
declare(strict_types=1);

namespace Nakima\MediaBundle\Provider;

/**
 * @author xgonzalez@nakima.es
 */

use MediaBundle\Entity\Media;
use MediaBundle\Entity\MediaProvider as MediaProviderEntity;
use Nakima\Utils\String\Text;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Symfony\Component\HttpFoundation\File\File;

class FileProvider extends MediaProvider
{

    protected $container;

    public function __construct(Container $container)
    {
        $this->container = $container;
    }

    public function buildMedia($file, $provider = null, \MediaBundle\Entity\Media $media = null): Media
    {

        if (!$media) {
            $media = new \ShopBundle\Entity\Media;
        }

        $media->setProvider(MediaProviderEntity::FILE());
        $media->setPrivateMetadata([]);
        $media->setSize($file->getClientSize());
        $media->setMetadata([]);
        $media->setMime($file->getMimeType());
        // $media->getName($image->getName());

        $baseName = Text::rstr(8);
        $name = $baseName.".".$file->guessClientExtension();
        $folder = sys_get_temp_dir();

        $file->move($folder, $name);

        $sFile = new File("$folder/$name", $name);
        $media->setFile($sFile);

        return $media;
    }

    protected function get($service)
    {
        return $this->container->get($service);
    }

    protected function has($service)
    {
        return $this->container->has($service);
    }
}

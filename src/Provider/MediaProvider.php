<?php
declare(strict_types=1);

namespace Nakima\MediaBundle\Provider;

/**
 * @author xgonzalez@nakima.es
 */

use MediaBundle\Entity\Media;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;

class MediaProvider
{

    protected $container;

    public function __construct(Container $container)
    {
        $this->container = $container;
    }

    public function buildMedia($file, $provider = null, \MediaBundle\Entity\Media $media = null): Media
    {

        $shortProvider = $provider;

        if (!$provider) {
            $provider = 'file';
            $shortProvider = 'file';
        }

        if (!$media) {
            $media = new \MediaBundle\Entity\Media;
        }

        if (!strpos($provider, '.')) {
            $provider = "nakima.media.provider.$provider";
        }

        if ($this->has($provider)) {
            $mediaProvider = $this->get($provider);
            if ($mediaProvider instanceof MediaProvider) {
                return $mediaProvider->buildMedia($file, $provider, $media);
            } else {
                throw new \Exception("Media provider not inherits '\Nakima\MediaBundle\MediaProvider'.");
            }
        } else {
            throw new \Exception("Media provider '$provider' not found");
        }

    }

    protected function get($service)
    {
        return $this->container->get($service);
    }

    protected function has($service)
    {
        return $this->container->has($service);
    }
}

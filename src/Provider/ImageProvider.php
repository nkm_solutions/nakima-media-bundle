<?php
declare(strict_types=1);

namespace Nakima\MediaBundle\Provider;

/**
 * @author xgonzalez@nakima.es
 */

use MediaBundle\Entity\MediaProvider as MediaProviderEntity;
use Nakima\Utils\File\Image;
use Nakima\Utils\String\Text;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Symfony\Component\HttpFoundation\File\File;

class ImageProvider extends MediaProvider
{

    protected $container;

    public function __construct(Container $container)
    {
        $this->container = $container;
    }

    public function buildMedia(
        $file,
        $provider = null,
        \MediaBundle\Entity\Media $media = null
    ): \MediaBundle\Entity\Media {

        $image = null;

        if (is_string($file)) {
            $image = Image::createFromString($file);
        }

        if (!$image) {
            $image = new Image($file);
        }

        $media->setProvider(MediaProviderEntity::IMAGE());
        $media->setPrivateMetadata([]);
        $media->setSize($image->getFileSize());
        $media->setMetadata(
            [
                'with' => $image->getSize()[0],
                'height' => $image->getSize()[1],
            ]
        );
        $media->setMime($image->getMimeType());
        $media->getName($image->getName());

        $baseName = Text::rstr(8);
        $name = $baseName.".".$image->getExtension();
        $folder = sys_get_temp_dir();

        $image->upload($folder, $name, true);

        $sFile = new File("$folder/$name", $name);

        $media->setFile($sFile);

        return $media;
    }

    protected function get($service)
    {
        return $this->container->get($service);
    }

    protected function has($service)
    {
        return $this->container->has($service);
    }
}
